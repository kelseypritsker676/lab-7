/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7part2Before;

import lab7part2Before.Movie;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kelsey.pritsker676
 */
public class MovieTest {
    
    public MovieTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getPriceCode method, of class Movie.
     */
    @Test
    public void testGetPriceCode() {
        System.out.println("getPriceCode");
        Movie instance = new Movie("Princess Bride",0);
        int expResult = 0;
        int result = instance.getPriceCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPriceCode method, of class Movie.
     */
    @Test
    public void testSetPriceCode() {
        System.out.println("setPriceCode");
        int arg = 0;
        Movie instance = new Movie("Shrek 3",3);
        instance.setPriceCode(arg);
    }

    /**
     * Test of getTitle method, of class Movie.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        Movie instance = new Movie("The Princess Bride",0);
        String expResult = "The Princess Bride";
        String result = instance.getTitle();
        assertEquals(expResult, result);
    }
}