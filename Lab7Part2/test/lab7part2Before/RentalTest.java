/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7part2Before;

import lab7part2Before.Rental;
import lab7part2Before.Movie;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kelsey.pritsker676
 */
public class RentalTest {
    
    public RentalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getDaysRented method, of class Rental.
     */
    @Test
    public void testGetDaysRented() {
        System.out.println("getDaysRented");
        Rental instance = new Rental(new Movie("Princess Bride",0),5);
        int expResult = 5;
        int result = instance.getDaysRented();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMovie method, of class Rental.
     */
    @Test
    public void testGetMovie() {
        System.out.println("getMovie");
        Movie mov = new Movie("Princess Bride",0);
        Rental instance = new Rental(mov,5);
        Movie expResult = mov;
        Movie result = instance.getMovie();
        assertEquals(expResult, result);
    }
}