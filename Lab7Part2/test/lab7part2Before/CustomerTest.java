/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7part2Before;

import lab7part2Before.Rental;
import lab7part2Before.Movie;
import lab7part2Before.Customer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kelsey.pritsker676
 */
public class CustomerTest {
    
    public CustomerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of addRental method, of class Customer.
     */
    @Test
    public void testAddRental() {
        System.out.println("addRental");
        Movie mov = new Movie("Princess Bride",0);
        Rental arg = new Rental(mov,5);
        Customer instance = new Customer("Jane");
        instance.addRental(arg);
    }

    /**
     * Test of getName method, of class Customer.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Customer instance = new Customer("Jane");
        String expResult = "Jane";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of statement method, of class Customer.
     */
    @Test
    public void testStatement() {
        System.out.println("statement");
        Customer instance = new Customer("Jane");
        Customer secondInstance = new Customer("Jane");
        String expResult = secondInstance.statement();
        String result = instance.statement();
        assertEquals(expResult, result);
    }
}