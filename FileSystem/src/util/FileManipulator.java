package util;

import java.io.File;

public class FileManipulator {

    private void recursiveFilePrinting(String path) {
        File filePath = new File(path);
        if (filePath.isFile()) {
            System.out.println(filePath.getPath());
        } else if (filePath.isDirectory()) {
            for (String directory : filePath.list()) {
                recursiveFilePrinting(path + filePath.separator + directory);
            }
        }
        if (!filePath.exists()) {
            System.out.println(path + " does not exist.");
        }
    }
}
