/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceextraction;

/**
 *
 * @author kelsey.pritsker676
 */
public interface IDatabaseAccess extends IDataAnalyzer {

    void fetchData();
    
}
